# README #

# App Development: Android #
## Fall 2015: The puzzle game Dots ##
### Students: G�sli Rafn Gudmundsson & J�n Reginbald Ivarsson ###

### Description ###

The task is to make a playing game called Dots, based on the game Dots (by PlayDots Inc.).

Minimum functionality:

1. A home activity, from which the user can navigate to the other activities, e.g. buttons for 'Play', 'Options', etc.

2. An activity where the user can configure various options of the game. This should include at least the option to reset the high-score list, but should also include other configuration options (of our choice; e.g. sound, vibrations, colors, etc.).

3. A board for playing the puzzle game, which includes:
	a. A graphical display using a touch-based interface for playing the game.
	b. Use of sound and/or vibration.
	c. The ability to use at least two different board sizes (where 6x6 should be the default size).
	d. Fice different color of dots, to appear randomly.

4. The user should be given a fixed number (30) of moves for solving a puzzle.

5. An activity using a list (grid) for listing high-scores, different list for each board size. The high-scores should be kept.

6. The app should run robustly and look good on different sized displays.

Additional functionality (examples):

1. Additional play modes, e.g. fixed time per puzzle (in addition to fixed moves).

2. More advanced animations and/or sound features.

3. There is no need to use the 'add-on' features in the basic implementation (i.e. the buttons for more moves, shrinkers and expanders), but may be used as an added functionality.

Also, free to add even more useful functionality, but correctness before extra functionality.
