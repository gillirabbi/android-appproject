package com.appproject.dots;

import java.io.Serializable;


public class Record implements Serializable {

    private String m_name;
    private String m_score;

    Record(String name, String score) {
        this.m_name = name;
        this.m_score = score;
    }

    String getName() {
        return m_name;
    }

    String getScore() {
        return m_score;
    }

    @Override
    public String toString() {
        return m_name + " " + m_score;
    }
}