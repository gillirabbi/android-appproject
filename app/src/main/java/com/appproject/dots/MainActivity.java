package com.appproject.dots;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    private Boolean m_use_sound = false;
    SharedPreferences m_SP;
    MediaPlayer m_sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_sound = MediaPlayer.create(this, R.raw.sound);
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_use_vibrator = m_SP.getBoolean("vibrate", false);
        m_use_sound = m_SP.getBoolean("sound", false);
    }

    public void Play(View view) {

        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }

        startActivity(new Intent(MainActivity.this, GameActivity.class));
    }

    public void Scores(View view) {
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }

        startActivity(new Intent(MainActivity.this, ScoreActivity.class));
    }

    public void Settings(View view) {
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }

        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
    }

    public void Timer(View view) {
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }

        startActivity(new Intent(MainActivity.this, TimerGameActivity.class));
    }
}
