package com.appproject.dots;

import java.util.ArrayList;

public interface OnMoveEventHandler {
    void circlesSelected(ArrayList<Circle> m_selectedCircles);
}