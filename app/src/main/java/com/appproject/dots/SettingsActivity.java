package com.appproject.dots;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.widget.Toast;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SettingsActivity extends PreferenceActivity {

    Preference m_resetHighScorePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setTitle("Settings");
        addPreferencesFromResource(R.xml.preferences);
        m_resetHighScorePref = findPreference("resetHighScore");
        resetHighScoreListener();
    }

    public void resetHighScoreListener() {
        m_resetHighScorePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try {
                    FileOutputStream fos = openFileOutput( "records.ser", Context.MODE_PRIVATE);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(new HighScores());
                    oos.close();
                    fos.close();
                }
                catch ( IOException e ) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), "High score reset", Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }
}

