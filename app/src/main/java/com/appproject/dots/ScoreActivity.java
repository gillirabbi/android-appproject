package com.appproject.dots;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class ScoreActivity extends AppCompatActivity {

    SharedPreferences m_SP;

    private ListView m_listView;

    private Button m_grid_score_button;
    private TextView m_subtitle;

    private HighScores m_data = new HighScores();

    private RecordAdapter m_adapter;

    private String m_showing_score_for;
    private String m_6x6_text;
    private String m_8x8_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        setTitle("High Scores");

        m_SP = PreferenceManager.getDefaultSharedPreferences(this);

        m_6x6_text = getResources().getString(R.string.grid_score_6_button);
        m_8x8_text = getResources().getString(R.string.grid_score_8_button);

        m_listView = (ListView) findViewById(R.id.records);
        m_grid_score_button = (Button) findViewById(R.id.grid_score_button);
        m_subtitle = (TextView) findViewById(R.id.Score_subtitle);

        switch (Integer.parseInt(m_SP.getString("GRID_SIZE", "6"))){
            case 6:
                m_grid_score_button.setText(m_8x8_text);
                m_subtitle.setText(m_6x6_text);
                m_showing_score_for = m_6x6_text;
                m_adapter = new RecordAdapter(this, m_data.getRecordsGrid6());
                m_listView.setAdapter(m_adapter);
                break;
            case 8:
                m_grid_score_button.setText(m_6x6_text);
                m_subtitle.setText(m_8x8_text);
                m_showing_score_for = m_8x8_text;
                m_adapter = new RecordAdapter(this, m_data.getRecordsGrid8());
                m_listView.setAdapter(m_adapter);
                break;
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        readRecords();
    }

    void readRecords() {
        try {
            FileInputStream fis = openFileInput("records.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            HighScores records = (HighScores) ois.readObject();
            ois.close();
            fis.close();
            m_data.clear();
            for ( Record rec: records.getRecordsGrid6() ) {
                m_data.addRecordsGrid6(rec);
            }
            for ( Record rec: records.getRecordsGrid8() ) {
                m_data.addRecordsGrid8(rec);
            }
            m_adapter.notifyDataSetChanged();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void ChangeScores(View view) {
        if(m_showing_score_for.equals(m_6x6_text)){
            m_grid_score_button.setText(m_6x6_text);
            m_subtitle.setText(m_8x8_text);
            m_showing_score_for = m_8x8_text;
            m_adapter = new RecordAdapter(this, m_data.getRecordsGrid8());
            m_listView.setAdapter(m_adapter);
        }
        else{
            m_grid_score_button.setText(m_8x8_text);
            m_subtitle.setText(m_6x6_text);
            m_showing_score_for = m_6x6_text;
            m_adapter = new RecordAdapter(this, m_data.getRecordsGrid6());
            m_listView.setAdapter(m_adapter);
        }
    }
}
