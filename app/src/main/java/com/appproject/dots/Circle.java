package com.appproject.dots;

public class Circle {
    private int m_x;
    private int m_y;
    private int m_color;
    private float m_height;
    private float m_width;

    private boolean m_active;

    Circle(int x, int y, int color, float width, float height){
        this.m_x = x;
        this.m_y = y;
        this.m_color = color;
        this.m_width = width;
        this.m_height = height;
        this.m_active = true;
    }

    public boolean getActive(){ return m_active; }

    public void setActive(boolean active){ m_active = active; }

    public int getX(){
        return m_x;
    }

    public int getY(){
        return m_y;
    }

    public int getColor(){
        return m_color;
    }

    public void setColor(int color){ this.m_color = color; }

    public void setHeight(float height){ this.m_height = height; }

    public float getHeight(){ return m_height; }

    public void setWidth(float width){ this.m_width = width; }

    public float getWidth(){ return m_width; }

    @Override
    public String toString() {
        return "x: " + m_x + " y: " + m_y + " color: " + m_color;
    }
}
