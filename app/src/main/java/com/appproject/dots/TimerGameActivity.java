package com.appproject.dots;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

public class TimerGameActivity extends AppCompatActivity {

    private BoardView m_bv;
    private TextView m_display_current_score;
    private TextView m_display_timer;
    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    private Boolean m_use_sound = false;
    private SharedPreferences m_SP;
    private MediaPlayer m_sound;

    private Integer m_score = 0;
    private CountDownTimer m_timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer_game);
        setTitle("Quick Game");

        m_sound = MediaPlayer.create(this, R.raw.sound);
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        m_bv = (BoardView) findViewById(R.id.board_view);
        m_display_current_score = (TextView) findViewById(R.id.current_score);
        m_display_timer = (TextView) findViewById(R.id.timer);

        m_display_current_score.setText("Score: " + m_score);

        startTimer();

        m_bv.setMoveEventHandler(new OnMoveEventHandler() {
            @Override
            public void circlesSelected(ArrayList<Circle> m_selectedCircles) {
                m_score = m_score + m_selectedCircles.size() * 10;
                m_display_current_score.setText("Score: " + m_score);
            }
        });
    }

    private void startTimer(){
        m_timer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                m_display_timer.setText("Time: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                m_display_timer.setText("Time's up!");
                m_bv.game_over();
                timeIsUp();
            }
        }.start();
    }

    private void resetTimer() {
        m_timer.cancel();
        startTimer();
    }

    private void timeIsUp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Time's up!");
        builder.setMessage("You got: " + m_score + " points!");
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
            }
        });
        builder.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_use_vibrator = m_SP.getBoolean("vibrate", false);
        m_use_sound = m_SP.getBoolean("sound", false);

    }
    @Override
    protected  void onStop(){
        super.onStop();
        m_timer.cancel();
    }

    public void Restart(View view) {
        SoundAndVibrate();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
                m_score = 0;
                resetTimer();
                m_display_current_score.setText("Score: " + m_score);
                m_bv.restart();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void SoundAndVibrate(){
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }
    }
}
