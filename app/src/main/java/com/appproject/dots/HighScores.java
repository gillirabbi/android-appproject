package com.appproject.dots;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HighScores implements Serializable {
    private ArrayList<Record> m_records_grid6;
    private ArrayList<Record> m_records_grid8;

    HighScores(){
        m_records_grid6 = new ArrayList<>();
        m_records_grid8 = new ArrayList<>();
    }

    public void addRecordsGrid6(Record record) {
        m_records_grid6.add(record);
    }

    public void addRecordsGrid8(Record record) {
        m_records_grid8.add(record);
    }

    public ArrayList<Record> getRecordsGrid6() {
        return m_records_grid6;
    }

    public ArrayList<Record> getRecordsGrid8() {
        return m_records_grid8;
    }

    public void clear() {
        m_records_grid6.clear();
        m_records_grid8.clear();
    }

    public void sortAndDrop() {
        Collections.sort(m_records_grid6, new Comparator<Record>() {
            public int compare(Record obj1, Record obj2) {
                    return Integer.parseInt(obj2.getScore()) - Integer.parseInt(obj1.getScore());
                }
            });
        if(m_records_grid6.size() > 10){
            m_records_grid6.remove(m_records_grid6.size() - 1);
        }
        Collections.sort(m_records_grid8, new Comparator<Record>() {
            public int compare(Record obj1, Record obj2) {
                return Integer.parseInt(obj2.getScore()) - Integer.parseInt(obj1.getScore());
            }
        });
        if(m_records_grid8.size() > 10){
            m_records_grid8.remove(m_records_grid8.size() - 1);
        }
    }
}
