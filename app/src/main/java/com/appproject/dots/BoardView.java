package com.appproject.dots;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BoardView extends View {

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    private Boolean m_use_sound = false;

    private List<MediaPlayer> mediaPlayers = new ArrayList<>();

    private OnMoveEventHandler m_moveHandler = null;

    ValueAnimator m_animator = new ValueAnimator();

    private int [] colors = {Color.rgb(2, 102, 200), Color.rgb(249, 1, 1), Color.rgb(0, 147, 59), Color.rgb(242, 181, 15), Color.rgb(155, 89, 182)};

    private int m_cellWidth;
    private int m_cellHeight;

    private Rect m_rect = new Rect();
    private Paint m_paint = new Paint();

    private RectF m_circle = new RectF();
    private Paint m_paintCircle = new Paint();

    private Path m_path = new Path();
    private Paint m_paintPath = new Paint();

    private List<Point> m_cellPath = new ArrayList<>();

    boolean m_moving = false;

    private int NUM_CELLS = 6;

    private Circle[][] m_circleMatrix;

    private ArrayList<Circle> m_selectedCircles;

    private boolean m_game_over = false;
    private boolean m_disable_touch = false;

    // CAT MODE
    private boolean m_cat_enabled = false;
    private Bitmap[] m_b;

    /*
     * Constructor for the class
     */
    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        m_vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        SharedPreferences m_SP = PreferenceManager.getDefaultSharedPreferences(getContext());
        m_use_vibrator = m_SP.getBoolean("vibrate", false);
        m_use_sound = m_SP.getBoolean("sound", false);
        m_cat_enabled = m_SP.getBoolean("cat_mode", false);
        NUM_CELLS = Integer.parseInt(m_SP.getString("GRID_SIZE", "6")); //todo find a better solution

        if(m_cat_enabled){
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.meow1_final));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.meow2_final));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.meow3_final));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.meow4_final));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.meow5_final));
        }
        else{
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ1));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ2));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ3));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ4));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ5));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ6));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ7));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ8));
            mediaPlayers.add(MediaPlayer.create(getContext(), R.raw.organ9));
        }

        m_paint.setColor(Color.BLACK);
        m_paint.setStyle(Paint.Style.STROKE);
        m_paint.setStrokeWidth(2);
        m_paint.setAntiAlias(true);

        m_paintCircle.setColor(Color.RED);
        m_paintCircle.setStyle(Paint.Style.FILL_AND_STROKE);
        m_paintCircle.setAntiAlias(true);

        m_paintPath.setColor(Color.BLACK);

        m_paintPath.setStrokeJoin(Paint.Join.ROUND);
        m_paintPath.setStrokeCap(Paint.Cap.ROUND);
        m_paintPath.setStyle(Paint.Style.STROKE);
        m_paintPath.setAntiAlias(true);

        m_circleMatrix = new Circle[NUM_CELLS][NUM_CELLS];
        m_selectedCircles = new ArrayList<>();

        if(m_cat_enabled){
            m_b = new Bitmap[5];
            m_b[0] = BitmapFactory.decodeResource(getResources(), R.drawable.cat1_final);
            m_b[1] = BitmapFactory.decodeResource(getResources(), R.drawable.cat2_final);
            m_b[2] = BitmapFactory.decodeResource(getResources(), R.drawable.cat3_final);
            m_b[3] = BitmapFactory.decodeResource(getResources(), R.drawable.cat4_final);
            m_b[4] = BitmapFactory.decodeResource(getResources(), R.drawable.cat5_final);
        }
    }

    @Override
    protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width  = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int height = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int size = Math.min(width, height);
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(),
                size + getPaddingTop() + getPaddingBottom());
        m_paintPath.setStrokeWidth(height / 50);
    }

    @Override
    protected void onSizeChanged( int xNew, int yNew, int xOld, int yOld ) {
        int   boardWidth = (xNew - getPaddingLeft() - getPaddingRight());
        int   boardHeight = (yNew - getPaddingTop() - getPaddingBottom());

        m_cellWidth = boardWidth / NUM_CELLS;
        m_cellHeight = boardHeight / NUM_CELLS;

        for(int i = 0; i < NUM_CELLS; ++i){
            for(int j = 0; j < NUM_CELLS; ++j){
                int x = getPaddingLeft() + j * m_cellWidth + m_cellWidth/2;
                int y = getPaddingTop() + i * m_cellHeight + m_cellHeight/2;
                m_circleMatrix[i][j] = new Circle(x,y,randomColor(colors), m_cellWidth * 0.3f, m_cellHeight * 0.3f);
            }
        }
        //if(m_cat_enabled){
        //    for (int i = 0; i < 5; i++){
        //        m_b[i] = Bitmap.createScaledBitmap(m_b[i], m_cellWidth, m_cellHeight, true);
        //    }
        //}

    }

    @Override
    protected void onDraw(Canvas canvas ) {
        canvas.drawRect(m_rect, m_paint);

        if (!m_cellPath.isEmpty()){
            m_path.reset();
            Point point = m_cellPath.get(0);
            m_path.moveTo(colToX(point.x) + m_cellWidth / 2, rowToY(point.y) + m_cellHeight / 2);
            for (int i = 1; i<m_cellPath.size(); ++i){
                point = m_cellPath.get(i);
                m_path.lineTo(colToX(point.x) + m_cellWidth / 2, rowToY(point.y) + m_cellHeight / 2);
            }
            canvas.drawPath(m_path, m_paintPath);
        }

        for(int i = 0; i < NUM_CELLS; ++i){
            for(int j = 0; j < NUM_CELLS; ++j){
                Circle c = m_circleMatrix[i][j];
                setCircleColor(c.getColor());
                m_circle.set(c.getX(), c.getY(), c.getX() + m_cellWidth, c.getY() + m_cellHeight);
                m_circle.inset(c.getWidth(), c.getHeight());
                snapToGrid(m_circle);
                if(m_cat_enabled){
                    float cat_width = c.getWidth()*3;
                    float cat_height = c.getHeight()*3;
                    if(c.getColor() == Color.rgb(2, 102, 200)){
                        canvas.drawBitmap(
                                getResizedBitmap(m_b[0], cat_width, cat_height),
                                c.getX() - cat_width / 2,
                                c.getY() - cat_height / 2,
                                m_paintCircle
                        );

                    } else if(c.getColor() == Color.rgb(249, 1, 1)){
                        canvas.drawBitmap(
                                getResizedBitmap(m_b[1], cat_width, cat_height),
                                c.getX() - cat_width / 2,
                                c.getY() - cat_height / 2,
                                m_paintCircle
                        );
                    } else if(c.getColor() == Color.rgb(0, 147, 59)){
                        canvas.drawBitmap(
                                getResizedBitmap(m_b[2], cat_width, cat_height),
                                c.getX() - cat_width / 2,
                                c.getY() - cat_height / 2,
                                m_paintCircle
                        );
                    } else if(c.getColor() == Color.rgb(242, 181, 15)){
                        canvas.drawBitmap(
                                getResizedBitmap(m_b[3], cat_width, cat_height),
                                c.getX() - cat_width / 2,
                                c.getY() - cat_height / 2,
                                m_paintCircle
                        );
                    } else {
                        canvas.drawBitmap(
                                getResizedBitmap(m_b[4], cat_width, cat_height),
                                c.getX() - cat_width / 2,
                                c.getY() - cat_height / 2,
                                m_paintCircle
                        );
                    }

                }
                else{
                    canvas.drawOval(m_circle, m_paintCircle);
                }
            }
        }


    }

    private void setCircleColor(int color){
        m_paintCircle.setColor(color);
    }

    private int xToMatrixX(int x){
        return (int) Math.floor(x / m_cellWidth);
    }

    private int yToMatrixY(int y){
        return (int) Math.floor(y / m_cellHeight);
    }

    private int xToCol(int x) {
        return (x - getPaddingLeft()) / m_cellWidth;
    }
    private int yToRow(int y) {
        return (y - getPaddingTop()) / m_cellHeight;
    }
    private int colToX(int col) {
        return col * m_cellWidth + getPaddingLeft();
    }
    private int rowToY(int row) {
        return row * m_cellHeight + getPaddingTop();
    }

    void snapToGrid(RectF circle){
        int col = xToCol((int) circle.left);
        int row = yToRow((int) circle.top);
        float x = colToX(col) + (m_cellWidth - circle.width()) / 2.0f;
        float y = rowToY(row) + (m_cellHeight - circle.height()) / 2.0f;
        circle.offsetTo(x, y);
    }



    private boolean isNeighbour(int y, int x){
        if(y != 0 && m_circleMatrix[y-1][x] == m_selectedCircles.get(m_selectedCircles.size() - 1)){
            return true;
        }
        if(y != NUM_CELLS - 1 &&m_circleMatrix[y+1][x] == m_selectedCircles.get(m_selectedCircles.size() - 1)){
            return true;
        }
        if(x != 0 && m_circleMatrix[y][x-1] == m_selectedCircles.get(m_selectedCircles.size() - 1)){
            return true;
        }
        if(x != NUM_CELLS - 1 && m_circleMatrix[y][x+1] == m_selectedCircles.get(m_selectedCircles.size() - 1)){
            return true;
        }
        return false;
    }

    @Override
    public  boolean onTouchEvent(MotionEvent event) {
        if(m_game_over) return true;
        if(m_disable_touch) return true;
        int x = (int) event.getX();
        int y = (int) event.getY();

        int xMax = getPaddingLeft() + m_cellWidth * NUM_CELLS;
        int yMax = getPaddingTop() + m_cellWidth * NUM_CELLS;
        x = Math.max(getPaddingLeft(), Math.min(x, (int) (xMax - m_circle.width())));
        y = Math.max(getPaddingTop(), Math.min(y, (int) (yMax - m_circle.height())));

        if(event.getAction() == MotionEvent.ACTION_DOWN){
            m_cellPath.add(new Point(xToCol(x), yToRow(y)));

            m_moving = true;
            m_selectedCircles.add(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)]);
            m_paintPath.setColor(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor());

            if (m_use_vibrator) {
                m_vibrator.vibrate(200);
            }
            if(m_use_sound) {
                if(m_cat_enabled) {
                    MediaPlayer mplayer;
                    if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(2, 102, 200)){
                        mplayer = mediaPlayers.get(0);
                    } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(249, 1, 1)){
                        mplayer = mediaPlayers.get(1);
                    } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(0, 147, 59)){
                        mplayer = mediaPlayers.get(2);
                    } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(242, 181, 15)){
                        mplayer = mediaPlayers.get(3);
                    } else {
                        mplayer = mediaPlayers.get(4);
                    }

                    if (mplayer.isPlaying()) {
                        mplayer.pause();
                        mplayer.seekTo(0);
                    }

                    mplayer.start();
                } else {
                    getSound(m_selectedCircles.size(), event);
                }
            }
        }
        else if (event.getAction() == MotionEvent.ACTION_MOVE){
            if(m_moving && !m_selectedCircles.isEmpty()){
                if (!m_cellPath.isEmpty()){
                    int col = xToCol(x);
                    int row = yToRow(y);
                    Point last = m_cellPath.get(m_cellPath.size() -1);
                    if (col != last.x || row != last.y) {
                        if (yToMatrixY(y) < NUM_CELLS && xToMatrixX(x) < NUM_CELLS){
                            if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == m_selectedCircles.get(0).getColor()){
                                if(isNeighbour(yToMatrixY(y),xToMatrixX(x)) && !m_selectedCircles.contains(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)])){
                                    m_selectedCircles.add(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)]);

                                    if(m_use_vibrator) {
                                        m_vibrator.vibrate(200);
                                    }

                                    if(m_use_sound) {
                                        if(m_cat_enabled) {
                                            MediaPlayer mplayer;
                                            if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(2, 102, 200)){
                                                mplayer = mediaPlayers.get(0);
                                            } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(249, 1, 1)){
                                                mplayer = mediaPlayers.get(1);
                                            } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(0, 147, 59)){
                                                mplayer = mediaPlayers.get(2);
                                            } else if(m_circleMatrix[yToMatrixY(y)][xToMatrixX(x)].getColor() == Color.rgb(242, 181, 15)){
                                                mplayer = mediaPlayers.get(3);
                                            } else {
                                                mplayer = mediaPlayers.get(4);
                                            }

                                            if (mplayer.isPlaying()) {
                                                mplayer.pause();
                                                mplayer.seekTo(0);
                                            }

                                            mplayer.start();
                                        } else {
                                            getSound(m_selectedCircles.size(), event);
                                        }
                                    }
                                    m_cellPath.add(new Point(col, row));
                                    invalidate();
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (event.getAction() == MotionEvent.ACTION_UP){
            m_moving = false;
            //snapToGrid(m_circle);
            if(m_selectedCircles.size() > 1){
                if ( m_moveHandler != null ) {
                    m_moveHandler.circlesSelected( m_selectedCircles);
                }

                animateShrink((ArrayList) m_selectedCircles.clone());

                if (m_use_vibrator) {
                    m_vibrator.vibrate(200);
                }
                if (m_use_sound) {
                     if(!m_cat_enabled) {
                        getSound(mediaPlayers.size(), event);
                    }
                }
            }
            else{
                m_selectedCircles.clear();
            }

            m_cellPath.clear();
            invalidate();
        }
        return true;
    }

    private void removeAndMove(){
        int[] number_of_circles_in_column = new int[NUM_CELLS];
        for(Circle circle : m_selectedCircles){
            number_of_circles_in_column[xToMatrixX(circle.getX())]++;
            circle.setWidth(m_cellWidth * 0.3f);        //reseting size
            circle.setHeight(m_cellHeight * 0.3f);      //reseting size
        }
        int[] lowest_in_column = new int[NUM_CELLS];
        for(Circle circle : m_selectedCircles){
            lowest_in_column[xToMatrixX(circle.getX())] = Math.max(lowest_in_column[xToMatrixX(circle.getX())], yToMatrixY(circle.getY()));
        }
        for(int i = 0; i < NUM_CELLS; i++){ //column
            for(int j = 0; j < number_of_circles_in_column[i]; j++){
                for(int q = lowest_in_column[i]; q >= j; q--){ //row
                    if (q == 0){
                        m_circleMatrix[q][i].setActive(false);
                    }
                    else{
                        m_circleMatrix[q][i].setColor(m_circleMatrix[q-1][i].getColor());
                        m_circleMatrix[q][i].setActive(true);
                        m_circleMatrix[q-1][i].setActive(false);
                    }
                }
            }
        }
        for(int i = 0; i < NUM_CELLS; ++i){
            for(int j = 0; j < NUM_CELLS; ++j){
                if(!m_circleMatrix[i][j].getActive()){
                    m_circleMatrix[i][j].setColor(randomColor(colors));
                    m_circleMatrix[i][j].setActive(true);
                }
            }
        }
        m_selectedCircles.clear();
        m_disable_touch = false;
    }


    //private void animateMovement(final ArrayList<Circle> circles){
    //    m_disable_touch = true;
    //    m_animator.removeAllUpdateListeners();
    //    m_animator.setDuration(3000);
    //    m_animator.setFloatValues(0.0f, 1.0f);
    //    m_animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
    //        @Override
    //        public void onAnimationUpdate(ValueAnimator animation) {
    //            float ratio = (float) animation.getAnimatedValue();
    //            //for (Circle circle : circles) {
    //            //    for(int i = 0; i < yToMatrixY(circle.getY()); i++){
    //            //        int yFrom = m_circleMatrix[i][xToMatrixX(circle.getX())].getY();
    //            //        int yTo = m_new_circleMatrix[i][xToMatrixX(circle.getX())].getY();
    //            //        m_circleMatrix[i][xToMatrixX(circle.getX())].setY((int) ((1.0 - ratio) * yFrom + ratio * yTo));
    //            //    }
    //            //}
    //            //for (int i = 0; i < NUM_CELLS; ++i) {
    //            //    for(int j = 0; j < NUM_CELLS; ++j){
    //            //        if(!m_new_circleMatrix[i][j].getActive()){
    //            //            int yFrom = m_circleMatrix[i][j].getY();
    //            //            int yTo = m_new_circleMatrix[i][j].getY();
    //            //            m_circleMatrix[i][j].setY((int) ((1.0 - ratio) * yFrom + ratio * yTo));
    //            //        }
    //            //    }
    //            //}
    //            invalidate();
    //        }
    //    });
    //    m_animator.start();
    //}

    private void animateShrink(final ArrayList<Circle> circles){
        m_disable_touch = true;
        m_animator.removeAllUpdateListeners();
        m_animator.setDuration(250);
        m_animator.setFloatValues(0.3f, 1.0f);
        m_animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float ratio = (float) animation.getAnimatedValue();
                for (Circle circle : circles) {
                    circle.setWidth(m_cellWidth * ratio);
                    circle.setHeight(m_cellHeight * ratio);
                }
                invalidate();
            }
        });
        Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {

            public void onAnimationStart(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {
            }

            public void onAnimationEnd(Animator animation) {
                removeAndMove();
            }

            public void onAnimationCancel(Animator animation) {
            }
        };

        m_animator.addListener(animatorListener);

        m_animator.start();
    }

    public static int randomColor(int[] colors) {
        int random = new Random().nextInt(colors.length);
        return colors[random];
    }

    public void getSound(int move, MotionEvent motionEvent) {
        MediaPlayer mplayer;

        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE && move >= mediaPlayers.size()) {
            mplayer = mediaPlayers.get(mediaPlayers.size() - 2);
        }else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            mplayer = mediaPlayers.get(mediaPlayers.size() - 1);
        } else {
            mplayer = mediaPlayers.get(move - 1);
        }

        if (mplayer.isPlaying()) {
            mplayer.pause();
            mplayer.seekTo(0);
        }

        mplayer.start();
    }

    public void setMoveEventHandler( OnMoveEventHandler handler ) {
        m_moveHandler = handler;
    }

    public void game_over() {
        m_game_over = true;
    }

    public void restart() {
        m_game_over = false;
        for(int i = 0; i < NUM_CELLS; ++i){
            for(int j = 0; j < NUM_CELLS; ++j){
                m_circleMatrix[i][j].setColor(randomColor(colors));
            }
        }
        invalidate();
    }

    public int getGridSize() {
        return NUM_CELLS;
    }

    public Bitmap getResizedBitmap(Bitmap bm, float newHeight, float newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = (newWidth) / width;
        float scaleHeight = (newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }
}