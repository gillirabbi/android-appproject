package com.appproject.dots;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    private BoardView m_bv;
    private TextView m_display_current_score;
    private TextView m_display_moves;
    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    private Boolean m_use_sound = false;
    private SharedPreferences m_SP;
    private MediaPlayer m_sound;

    private int m_moves = 30;
    private Integer m_score = 0;
    private String m_username = "";

    private int m_gridSize;

    private HighScores highScore = new HighScores();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        setTitle("Game");

        m_sound = MediaPlayer.create(this, R.raw.sound);
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        m_bv = (BoardView) findViewById(R.id.board_view);
        m_display_current_score = (TextView) findViewById(R.id.current_score);
        m_display_moves = (TextView) findViewById(R.id.moves);

        m_gridSize = m_bv.getGridSize();

        m_display_moves.setText("Moves: " + m_moves);
        m_display_current_score.setText("Score: " + m_score);

        m_bv.setMoveEventHandler(new OnMoveEventHandler() {
            @Override
            public void circlesSelected(ArrayList<Circle> m_selectedCircles) {
                m_moves -= 1;
                m_display_moves.setText("Moves: " + m_moves);
                m_score = m_score + m_selectedCircles.size() * 10;
                m_display_current_score.setText("Score: " + m_score);
                if (m_moves <= 0) {
                    m_bv.game_over();
                    showDialog();
                }

            }
        });
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText user = new EditText(this);
        TextView score = new TextView(this);
        LinearLayout layout = new LinearLayout(this);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(params);
        layout.setGravity(Gravity.CLIP_VERTICAL);
        layout.setPadding(2, 2, 2, 2);

        score.setText("Score: " + m_score);
        score.setPadding(40, 40, 40, 40);
        score.setGravity(Gravity.LEFT);
        score.setTextSize(20);

        LinearLayout.LayoutParams scoreParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        scoreParams.bottomMargin = 5;
        layout.addView(score, scoreParams);
        layout.addView(user, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        builder.setTitle("Game Over!");
        builder.setView(layout);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_username = user.getText().toString();

                SoundAndVibrate();
                readRecords();
                writeRecords();
                startActivity(new Intent(GameActivity.this, ScoreActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_use_vibrator = m_SP.getBoolean("vibrate", false);
        m_use_sound = m_SP.getBoolean("sound", false);
    }

    public void Restart(View view) {
        SoundAndVibrate();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
                m_moves = 30;
                m_score = 0;
                m_display_moves.setText("Moves: " + m_moves);
                m_display_current_score.setText("Score: " + m_score);
                m_bv.restart();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoundAndVibrate();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void SoundAndVibrate(){
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        if (m_use_sound) {
            m_sound.start();
        }
    }

    private void readRecords() {
        try {
            FileInputStream fis = openFileInput("records.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            HighScores records = (HighScores) ois.readObject();
            ois.close();
            fis.close();
            highScore.clear();
            for ( Record rec: records.getRecordsGrid6() ) {
                highScore.addRecordsGrid6( rec );
            }
            for ( Record rec: records.getRecordsGrid8() ) {
                highScore.addRecordsGrid8(rec);
            }
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    private void writeRecords() {
        try {
            FileOutputStream fos = openFileOutput( "records.ser", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            if (m_username.equals(""))
                m_username = "Anonymous";

            if (m_gridSize == 6) {
                highScore.addRecordsGrid6(new Record(m_username, m_score.toString()));

            } else {
                highScore.addRecordsGrid8(new Record(m_username, m_score.toString()));
            }
            highScore.sortAndDrop();
            oos.writeObject(highScore);
            oos.close();
            fos.close();
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
